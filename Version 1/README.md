The very first version of the script managing printing according to amount of pages of a Word document.

Install
Copy template.dotx to c:/temp/ before using.

The script sends tasks to a printer according to values user input before a process starts and divides them into different parts.

Example:

input1: 10 15
input2: 4fg

According to values the script sends following tasks to printer
4f x 10 copies
fg x 10 copies

And every of them will be named as the number of the class with the letter.
