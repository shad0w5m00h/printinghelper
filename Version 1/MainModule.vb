' PrintTests -> requestData (if data is right) -> PrintTask

Sub PrintTests()
    Dim NumPages As Long
    ' Count pages of document
    NumPages = ActiveDocument.ActiveWindow.Panes(1).Pages.Count
    
    If NumPages > 2 And NumPages Mod 4 > 0 Then
        MsgBox "Sorry, total of pages is not a multiple of 4."
        Exit Sub
    Else
        With requestData.ComboBox1 ' combo box about duplex's mode
            .AddItem "1"
            .AddItem "2"
            .ListIndex = 1 ' a value of duplex's mode by default
        End With
        With requestData.ComboBox2 ' combo box about edge's printing
            .AddItem "Simplex"
            .AddItem "Long edge"
            .AddItem "Short edge"
            .ListIndex = 1 ' a value of edge's printing by default
        End With
        requestData.Show
    End If
End Sub

Public Function PrintTask(ByRef NumbersArray() As String, NumberClass As String, _
    ByRef LettersArray() As String, ByVal modeDuplex As Integer, ByVal zoomForColumn As Integer)
    
    nameDocument = Left(Application.ActiveDocument.Name, Len(Application.ActiveDocument.Name) - 5)
    sPrinter = ActivePrinter ' we've got back printer had chosen before script was started
    
    'Choose needful printer
    'ActivePrinter = "HP LaserJet 700 color MFP M775 [A9E469] (êîïèÿ 1)"
    ActivePrinter = "NPIA9C443 (HP LaserJet MFP M725)" ' ??????????? ??? ???????? ?? ???? "??????? ?? ?????????" ? ??????????
    
    'Debug.Print ("DuplexMode: " & modeDuplex)
    'Debug.Print ("Pages on one page: " & zoomForColumn)
    
    Call SetPrinterDuplex(ActivePrinter, modeDuplex)
    
    For i = 0 To UBound(NumbersArray) ' we are going to repeat so many times as we have classes in the array called NumbersArray
        Documents.Open FileName:="C:\temp\template.dotx"
        ActiveDocument.PageSetup.Orientation = wdOrientLandscape
        ActiveDocument.Bookmarks("class").Range.Text = NumberClass & LettersArray(i)
        ActiveDocument.Bookmarks("AmountCopies").Range.Text = NumbersArray(i)
        ActiveDocument.PrintOut Background:=True, Copies:=1, Collate:=True, PrintZoomColumn:=1, PrintZoomRow:=1
        ActiveDocument.Close SaveChanges:=wdDoNotSaveChanges
        Tasks(nameDocument).Activate
        ActiveDocument.PrintOut Background:=True, Copies:=NumbersArray(i), Collate:=True, PrintZoomColumn:=zoomForColumn, PrintZoomRow:=1
    Next i
    
    Application.ActivePrinter = sPrinter
End Function